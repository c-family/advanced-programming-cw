/*
 ============================================================================
 Name        : CWK2Q3.c
 Author      : Anonymous (DO NOT CHANGE)
 Description :
 Implement your own Hash Table in C for storing and searching names, i.e. char
 arrays. In the event of collisions, you should use linear probing with an
 interval of 1. The hash function should be the sum of the ASCII values of the
 string modulo the size of the underlying data structure. Your Hash Table
 implementation should have the following interface:
	int hash_function(const char *key)
	void resize_map(int new_size)
	void add_to_map(const char *name)
	int remove_from_map(const char *name)
	int search_map(const char *name)
	void print_map()

 ============================================================================
*/

#include <stdio.h>
#include <stdlib.h>

char **hash_map;  // this is where you should store your names
int size = 0;     // size of the hash_map
int elements = 0; // number of elements in the hashmap at any given time

/**
 * Hash function computed as sum of the ASCII values of the string modulo the size of the hashmap data structure
 * @param key the key to use for the computation
 * @return an integer representing the derived index location
 */
int hash_function(const char *key) {
    int asciiSum = 0;
    for (int i = 0; key[i] != '\0'; i++) {
        asciiSum += (unsigned char) key[i];
    }
    return asciiSum % size;
}

// Note that not more than 70% of map size should be used up.
void resize_map(int new_size) {
    if (new_size <= elements) {
        printf("%d is less than the elements in hash map so some values may be lost. Please resize with integer > %d",
               new_size, elements);
        return;
    }
    // if new_size will cause more than 70% to be used, double the size
    if ((float) elements / (float) new_size > 0.7) {
        new_size *= 2;
    }
    // reserve new_size block of memory
    char **new_hash_map = (char **) (malloc(new_size * sizeof(char *)));
    int original_size = size;
    // new size will be used in rehashing the function
    size = new_size;

    //loop through existing,if element exists, rehash the value and add to new hashmap
    for (int i = 0; i < original_size; i++) {
        if (hash_map[i] != 0) {
            int location = hash_function(hash_map[i]);
            new_hash_map[location] = hash_map[i];
        }
    }
    free(hash_map);
    hash_map = new_hash_map;
}

void add_to_map(const char *name) {
    // if hash_map is 70% full, resize map
    if ((float) elements / (float) size >= 0.7) {
        resize_map(size++);
    }
    int location = hash_function(name);
    do {
        // if collision (if value is present), perform linear probing - try place in the space next to location (increment)
        if (hash_map[location] == 0) {
            hash_map[location] = name;
            elements++;
            break;
        } else {
            location++;
        }
    } while (location < size);
}

int remove_from_map(const char *name) {
    int location = hash_function(name);
    if (hash_map[location] == name) {
        hash_map[location] = 0;
        elements--;
        return 1;
    }
    return 0;
}

int search_map(const char *name) {
    int location = hash_function(name);
    do {
        if (hash_map[location] == name) {
            return 1;
        } else {
            //linear probing - move along 1 each time
            location++;
        }
    } while (location < size);
    return 0;
}

void print_map() {
    for (int i = 0; i < size-1; i++) {
        if (hash_map[i] != 0) printf("%s, ", hash_map[i]);
    }
    printf("\n");
}

int main(int argc, char *argv[]) {
    char *stringOne = "#Hello world";
    char *stringTwo = "How are you?";
    char *stringThree = "Be the best you...!!";
    char *stringFour = "Be kind to yourself";
    char *stringFive = "Principles of Programming 2";

    resize_map(6);
    add_to_map(stringOne);
    add_to_map(stringTwo);
    add_to_map(stringOne);
    add_to_map(stringThree);
    add_to_map(stringFour);
    add_to_map(stringFive);
    print_map();

    int ret = search_map(stringOne);
    if (ret)
        printf("Found %s!\n", stringOne);

    remove_from_map(stringThree);

    ret = search_map(stringFive);
    if (ret)
        printf("Found %s!\n", stringFive);
    print_map();

    add_to_map(stringThree);
    print_map();

    return EXIT_SUCCESS;
}

/** The main function below demonstrates reading from the sample text file for testing. **/
//int main(int argc, char *argv[]) {
//    resize_map(6);
//
//    char *filename = "./names.txt";
//    FILE *fp;
//    char buff[255];
//    fp = fopen(filename, "r");
//
//    int i = 0;
//    while(i>=0) {
//        // read file line by line irrespective of whitespace chars or commas
//        i = fscanf(fp, "%[^ \t\n\r\v\f,],%*c", buff);
//        printf("%s\n", buff);
//        // add word to hash_map
//        add_to_map(buff);
//    }
//    print_map();
//
//    return EXIT_SUCCESS;
//}


