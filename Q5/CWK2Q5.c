/*
 ============================================================================
 Name        : CWK2Q5.c
 Author      : Anonymous (DO NOT CHANGE)
 Description :
 Implement an algorithm in C which given a file containing a block of text as
 input, redacts all words from a given set of “redactable” words (also from a
 file), and outputs the result to a file called “result.txt”. For example,
 given the block of text:
    The quick brown fox jumps over the lazy dog

 and the redactable set of words:
    the, jumps, lazy

 the output text in “result.txt” should be
    *** quick brown fox ***** over *** **** dog

 Note that the number of stars in the redacted text is the same as the number
 of letters in the word that has been redacted, and that capitalization is
 ignored. You should not use any of the string libraries to answer this
 question. You should also test your program using the example files
 provided.
 ============================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

char *redact_word(const char *word);

char **redact_array;
int arr_string_size = 255;
int elements = 0;

/**
 * Opens a file with the given filename and exits upon error
 * @param filename - name of the file to be opened. This is created if it doesnt exist
 * @param mode - mode of file: "r" for read-only or "w" for write-only
 * @return a FILE pointer upon successful opening of file. Otherwise, returns NULL
 */
FILE *open_file(const char *filename, char mode[]) {
    FILE *f_ptr;
    if ((f_ptr = fopen(filename, mode)) == NULL) {
        printf("Error! Failed to open file: %s\n", filename);
        exit(1); // unsuccessful termination
    }
    return f_ptr;
}

/**
 * Allocates size bytes of memory to an array of chars (strings).
 * @param size - the size of memory to allocate in bytes
 * @return a char pointer to the allocated memory
 */
char *make_string(int size) {
    return (char *) malloc(size * sizeof(char));
}

/**
 * Allocates size bytes of memory to an array of strings.
 * @param size - the size of memory to allocate in bytes
 * @return a string pointer to the allocated memory
 */
char **make_array(int size) {
    return (char **) malloc(size * sizeof(char *));
}

/**
 * Allocate memory to the strings in a specified array
 * @param array the array to allocate memory for
 */
void fill_array(char **array) {
    for (int i = 0; i < arr_string_size; i++) {
        char *c = make_string(i);
        array[i] = c;
    }
}

/**
 * Allocate new block of memory to the redact array
 * @param new_size the size to use to calculate the memory size
 */
void resize_redact_array(int new_size) {
    char **new_array = make_array(new_size);
    // reserve new blocks of memory for each string inside the array
    fill_array(new_array);

    for (int i = 0; i < elements; i++) {
        new_array[i] = redact_array[i];
    }
    // deallocate the memory allocation pointed to by the redact array.
    free(redact_array);
    redact_array = new_array;
}

/**
 * Populate the redact_array with contents from the given filename
 * @param filename - text file containing words to be redacted
 */
void populate_redact_text_array(const char *filename) {
    int arr_size = 10;
    FILE *in_file = open_file(filename, "r");

    redact_array = make_array(arr_size);
    fill_array(redact_array);

    int n_read;
    do {
        if (elements >= arr_size) {
            arr_size *= 2;
            resize_redact_array(arr_size);
        }
        // read each string from the file and insert into the index in redact_array marked by elements
        // use "%[^ \t\n\r\v\f,],%*c" to read comma-separated strings
        n_read = fscanf(in_file, "%s", redact_array[elements]);
        if (n_read < 0) break;
        elements++;
    } while (n_read > 0);

    fclose(in_file);
}

void print_array(char **array, int size) {
    printf("Redact words array: [");
    for (int i = 0; i < size - 1; i++) {
        printf("%s, ", array[i]);
    }
    printf("%s]\n", array[size - 1]);
}


/**
 * Start from a given index and replace the first substring that matches the specified sequence of characters.
 * @param str - the string where the replacement is performed.
 * @param startIndex - the index to start replacing the substring from
 * @param seq - the sequence of characters to replace in the string
 */
void replace(char *str, int startIndex, const char *seq) {
    for (int i = 0; seq[i] != '\0'; i++) {
        str[startIndex] = seq[i];
        startIndex++;
    }
}

/**
 * Replaces each substring of this string that matches the specified sequence of characters.
 * @param str - the string where the replacement is performed.
 * @param seq - the sequence of characters to replace in the string
 */
void replaceAllIfExists(char *str, char *seq) {
    int seqLen = 0;
    for (int strLen = 0; str[strLen] != '\0'; strLen++) {
        char strChar = str[strLen], seqChar = seq[seqLen];
        // if the chars are equal or if they are same alphabets irrespective of casing, keep count with seqLen
        if ((strChar == seqChar) || (isalpha(strChar) && isalpha(seqChar) && tolower(strChar) == tolower(seqChar))) {
            seqLen++;
            if (seq[seqLen] == '\0') {
                // get seq seqLen (seqLen-1) and subtract by seqLen of str so far (strLen) to get startIndex
                int startIndex = abs((seqLen - 1) - strLen);
                char *redacted = redact_word(seq);
                replace(str, startIndex, redacted);
                // reset to zero to commence check for another occurrence of the word
                seqLen = 0;
            }
            continue;
        }
        // reset length to 0 if a consecutive non-matching char is found
        if (seqLen > 0) seqLen = 0;
    }
}

/**
 * Return the redact form of the given word. This is derived by replacing all chars in the word with *
 * @param word word to be redacted
 * @return the converted redact form of the word using *
 */
char *redact_word(const char *word) {
    char *redacted = make_string(arr_string_size);
    for (int i = 0; word[i] != '\0'; i++) {
        redacted[i] = '*';
    }
    return redacted;
}

/**
 * Reads input text files, redact appropriate words and writes result to 'result.txt' file
 * @param text_filename - Input text file to be redacted
 * @param redact_words_filename - text file containing words to be redacted
 */
void redact_words(const char *text_filename, const char *redact_words_filename) {
    populate_redact_text_array(redact_words_filename);
    print_array(redact_array, elements);

    FILE *in_file = open_file(text_filename, "r");
    FILE *out_file = open_file("result.txt", "w");

    int string_size = 1024; //arbitrary buffer size that will hold a line of string
    char string_buf[string_size];
    char *line;

    // read each line of file till NULL/EOF is reached
    while ((line = fgets(string_buf, string_size, in_file)) != NULL) {
        if (line[0] != '\n') {
            //if a redact-able word exists in line, replace all occurrences of the word in the line with its redact form (with *)
            for (int i = 0; i < elements; i++) {
                char *word = redact_array[i];
                replaceAllIfExists(line, word);
            }
        }
        // print to output file
        fprintf(out_file, "%s", line);
    }
    free(line);
    line = NULL;
    fclose(in_file);
    fclose(out_file);
}

int main(int argc, char *argv[]) {
    const char *input_file = "./debate.txt";
    const char *redact_file = "./redact.txt";
    redact_words(input_file, redact_file);
    return EXIT_SUCCESS;
}
