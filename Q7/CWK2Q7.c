/*
 ============================================================================
 Name        : CWK2Q7.c
 Author      : Anonymous (DO NOT CHANGE)
 Description :
 Implement a Columnar Transposition Cipher in C to encrypt a message of any
 length. A Columnar Transposition Cipher is transposition cipher that follows
 a simple rule for mixing up the characters in the plaintext to form the
 ciphertext.

 As an example, to encrypt the message ATTACKATDAWN with the keyword KEYS,
 we first write out our message as shown below,
    K	E	Y	S
    A	T	T	A
    C	K	A	T
    D	A	W	N

 Note: if the message to encode does not fit into the grid, you should pad
 the message with x's or random characters for example, ATTACKNOW with the
 keyword KEYS might look like below,
    K	E	Y	S
    A	T	T	A
    C	K	N	O
    W	X	X	X

 Once you have constructed your keyMap, the columns are now reordered such
 that the letters in the keyword are ordered alphabetically,
    E	K	S	Y
    T	A	A	T
    K	C	T	A
    A	D	N	W

 The ciphertext is now read off along the columns, so in our example above,
 the ciphertext is TAATKCTAADNW.

 You should demonstrate your implementation by encrypting the file in the
 folder Q7 using the keyword - LOVELACE.

 ============================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/**
 * Allocates string_size bytes of memory to an array of chars (strings).
 * @param string_size - the string_size of memory to allocate in bytes
 * @return a char pointer to the allocated memory
 */
char *make_string(int string_size) {
    char *string;
    int i = 0;
    do {
        string = (char *) malloc(string_size * sizeof(char));
        i++;
        // exit if memory is unable to be reserved after 5 attempts
        if (i > 5) exit(1);
    } while (string == NULL);

    return string;
}

/**
 * Create a pointer to char value and reserves allocated memory till its freed up
 * @param value
 * @return
 */
char *make_char(char value) {
    char *ch;
    int i = 0;
    do {
        ch = (char *) malloc(sizeof(char));
        i++;
        // exit if memory is unable to be reserved after 5 attempts
        if (i > 5) exit(1);
    } while (ch == NULL);
    *ch = value;
    return ch;
}

/**
 * Calculate length of string
 * @param str string to determine the length
 * @return length of string as an integer
 */
int str_len(const char *str) {
    int length = 0;
    for (int i = 0; str[i] != '\0'; i++) {
        length++;
    }
    return length;
}


/**
 * Sorts a string and returns the sorted string
 * @param str the string to be sorted
 * @return the sorted string in alphabetical order
 */
char *sort_str(const char *str) {
    int len = str_len(str);
    char *sorted_str = make_string(len);
    // initialize sorted_str with contents of str
    for (int i = 0; i < len; i++) {
        sorted_str[i] = str[i];
    }
    for (int i = 0; i < len - 1; i++) {
        for (int j = i + 1; j < len; j++) {
            if (sorted_str[i] > sorted_str[j]) {
                char temp = sorted_str[i];
                sorted_str[i] = sorted_str[j];
                sorted_str[j] = temp;
            }
        }
    }
    return sorted_str;
}

/**
 * Returns index of a char value if this string contains the value, otherwise returns -1.
 * @param str string where index of the char is searched for
 * @param ch the char value to search for
 * @return index of ch in str if this string contains ch, -1 otherwise
 */
int index_of_char(const char *str, const char ch) {
    for (int i = 0; str[i] != '\0'; i++) {
        char strChar = str[i];
        // if the chars are equal or if they are same alphabets irrespective of casing, keep count with seqLength
        if ((strChar == ch) || (isalpha(strChar) && tolower(strChar) == tolower(ch))) {
            return i;
        }
    }
    return -1;
}

/**
 * Returns a plain string devoid of characters and punctuation
 * @param str
 * @return string containing only letters
 */
char *remove_chars_and_punctuations(const char *str) {
    char *new_str = make_string(str_len(str));
    char ch;
    int i = 0, j = 0;

    while ((ch = str[i]) != '\0') {
        // only add to new_str if letter
        if (isspace(ch) == 0 && ispunct(ch) == 0) {
            new_str[j] = (char) toupper(ch);
            j++;
        }
        i++;
    }
    return new_str;
}

/**
 * Read from file and populate message with contents from the given filename
 * @param filename - text file containing message
 * @return message as a string
 */
char *read_message_str(const char *filename) {
    FILE *f_ptr;
    if ((f_ptr = fopen(filename, "r")) == NULL) {
        printf("Error! Failed to open file: %s\n", filename);
        exit(1); // unsuccessful termination
    }

    int string_size = 1024; //arbitrary buffer size that will hold a line of string
    char string_buf[string_size];
    char *line, *message;

    message = make_string(4000);
    // read each line of file till NULL/EOF is reached
    while ((line = fgets(string_buf, string_size, f_ptr)) != NULL) {
        strcat(message, line);
    }
    free(line);
    line = NULL;
    return message;
}

/**
 * Encrypts file contents using specified key
 * @param message_filename filename to read message to encrypt from
 * @param key encryption key
 * @param result string pointer that points to the encrypted text
 */
void encrypt_columnar(const char *message_filename, const char *key, char **result) {
    // read filename into an string
    char *original_msg = read_message_str(message_filename);
    char *only_alpha_msg = remove_chars_and_punctuations(original_msg);

    int col, row;
    char *cipher;
    int msg_len = str_len(only_alpha_msg);

    //calculate column and row sizes of table. increase row if some chars did not cover msg length
    col = str_len(key);
    row = msg_len / col;
    if (msg_len % col == 1) row++;

    //pad with 'X' if msg length < table_size;
    int table_size = row * col;
    while (msg_len < table_size) {
        strcat(only_alpha_msg, "X");
        msg_len++;
    }
    char *sorted_key;
    sorted_key = sort_str(key);
    cipher = make_string(msg_len);

    for (int i = 0; i < col; i++) {
        //get original position of ith char in sorted key
        int index = index_of_char(key, sorted_key[i]);
        for (int j = 0; j < row; j++) {
            char *ch = make_char(only_alpha_msg[index]);
            // concatenate cipher with pointer to char (string) at current index in string
            strcat(cipher, ch);
            free(ch);
            ch = NULL;
            // make next index to be next position down the same column
            index = (index + col) % msg_len;
        }
    }
    // change value pointed to by result to cipher
    *result = cipher;
}

int main(int argc, char *argv[]) {
    const char *example_message = "./text.txt";
    const char *example_key = "LOVELACE";
    char *encrypted_message = NULL;
    encrypt_columnar(example_message, example_key, &encrypted_message);
    printf("Encrypted message = %s\n", encrypted_message);
    // insert more code here :-)
    return EXIT_SUCCESS;
}
