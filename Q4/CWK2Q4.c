/*
 ============================================================================
 Name        : CWK2Q4.c
 Author      : Anonymous (DO NOT CHANGE)
 Description :
 Implement your own XOR Linked List (https://en.wikipedia.org/wiki/XOR_linked_list)
 in C capable of storing names. Your implementation should have the following
 functions:
    void insert_string(const char* newObj)
	int insert_before(const char* before, const char* newObj)
	int insert_after(const char* after, const char* newObj)
	void remove_string(char* result)
	int remove_after(const char *after, char *result)
	int remove_before(const char *before, char *result)
    void print_list()

 ============================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void populate_remove_result(char *result, char *value);

struct Node *node_head;

/** Node structure of XOR linked list **/
struct Node {
    char *value;
    //XOR of next and previous node to get next node
    struct Node *xor_next_prev;
};

/** Helper function to return XOR of the 2 node addresses **/
struct Node *XOR(struct Node *node1, struct Node *node2) {
    return (struct Node *) ((uintptr_t) (node1) ^ (uintptr_t) (node2));
}

/** Node constructor **/
struct Node *make_node(const char *v) {
    struct Node *new_node;

    new_node = (struct Node *) malloc(sizeof(struct Node));
    if (new_node == NULL) {
        printf("Error! Failed to create Node. Please try again later");
        exit(1);//unsuccessful termination
    }
    new_node->value = v;
    // this field is the XOR of the current head and 'NULL'
    // new_node->xor_next_prev = XOR(node_head, NULL);// initialise
    new_node->xor_next_prev = NULL;// initialise

    return new_node;
}

struct Node *getNextNode(struct Node *current, struct Node *prev);

/**
 * Create a pointer to char value and reserves allocated memory till its freed up
 * @param value
 * @return
 */
char *make_char(char value) {
    char *ch;
    int i = 0;
    do {
        ch = (char *) malloc(sizeof(char));
        i++;
        // exit if memory is unable to be reserved after 5 attempts
        if (i > 5) exit(1);
    } while (ch == NULL);
    *ch = value;
    return ch;
}

void insert_string(const char *newObj) {
    // allocate memory for new node and set its value
    struct Node *new_node = make_node(newObj);
    new_node->xor_next_prev = XOR(node_head, NULL);// initialise

    // if linked list is not empty, update xor link value
    if (node_head != NULL) {
        // node_head->xor_next_prev is XOR of NULL and next. Get next node by then XOR-ing head->xor_next_prev with NULL
        struct Node *node2 = XOR(node_head->xor_next_prev, NULL);
        node_head->xor_next_prev = XOR(new_node, node2);
    }
    // update the head pointer
    node_head = new_node;
}

/** Get address of next node given current and previous nodes **/
struct Node *getNextNode(struct Node *current, struct Node *prev) {
    // XOR(next, prev) is equal to current->xor_next_prev
    // therefore, XOR(current->xor_next_prev, prev) = XOR(XOR(next, prev),prev) is equal to next
    return XOR(current->xor_next_prev, prev);
}

/** Insert node with newObj value before the node with 'before' value **/
int insert_before(const char *before, const char *newObj) {
    struct Node *current = node_head;
    struct Node *prev = NULL, *p_prev = NULL;
    struct Node *next;

    while (current != NULL) {
        if (strcmp(node_head->value, before) == 0) {
            insert_string(newObj);
            return 1;
        }
        next = getNextNode(current, prev);

        if (strcmp(before, current->value) == 0) {
            // allocate memory for new node and set its value
            struct Node *new_node = make_node(newObj);
            // place new node in between current and previous node
            new_node->xor_next_prev = XOR(prev, current);
            // update current node address
            current->xor_next_prev = XOR(new_node, next);
            // update previous node address
            prev->xor_next_prev = XOR(p_prev, new_node);
            return 1;
        }
        p_prev = prev;
        prev = current;
        current = next;
    }
    return 0;
}

/** Insert node with newObj value after the node with 'after' value **/
int insert_after(const char *after, const char *newObj) {
    struct Node *current = node_head;
    struct Node *prev = NULL;
    struct Node *next, *n_next;

    while (current != NULL) {
        next = getNextNode(current, prev);

        if (strcmp(after, current->value) == 0) {
            struct Node *new_node = make_node(newObj);

            if (next == NULL) {//end of list
                new_node->xor_next_prev = XOR(current, NULL);
                current->xor_next_prev = XOR(prev, new_node);
                return 1;
            }
            new_node->xor_next_prev = XOR(current, next);
            current->xor_next_prev = XOR(prev, new_node);
            n_next = getNextNode(next, current);// node after the next node
            next->xor_next_prev = XOR(new_node, n_next);
            return 1;
        }
        prev = current;
        current = next;
    }
    return 0;
}

/** Removes result from list **/
int remove_string(char *result) {
    struct Node *current = node_head;
    struct Node *prev = NULL, *p_prev = NULL;
    struct Node *next, *next_next = NULL;

    while (current != NULL) {
        next = getNextNode(current, prev);
        if (next != NULL) {
            next_next = getNextNode(next, current);
        }
        if (strcmp(result, current->value) == 0) {
            // deallocate current node address
            free(current);
            current = NULL;
            // update next and previous node addresses
            if (next != NULL) {
                next->xor_next_prev = XOR(prev, next_next);
            }
            prev->xor_next_prev = XOR(p_prev, next);
            printf("remove_string result is: %s\n", result);
            return 1;
        }
        p_prev = prev;
        prev = current;
        current = next;
    }
    return 0;
}

/** Removes result in-front of 'after' node **/
int remove_after(const char *after, char *result) {
    struct Node *current = node_head;
    struct Node *prev = NULL;
    struct Node *next, *next_next = NULL, *next_next_next = NULL;

    while (current != NULL) {
        //derive next 3 nodes
        next = getNextNode(current, prev);
        if (next != NULL) {
            next_next = getNextNode(next, current);
            if (next_next != NULL) {
                next_next_next = getNextNode(next_next, next); // node after the next after the next node
            }
        }
        //if match found
        if (strcmp(after, current->value) == 0) {
            if (next == NULL) {//end of list
                printf("Nothing to remove after %s", after);
                return 0;
            }
            populate_remove_result(result, next->value);

            current->xor_next_prev = XOR(prev, next_next);
            if (next_next != NULL) {
                next_next->xor_next_prev = XOR(current, next_next_next);
            }
            return 1;
        }
        prev = current;
        current = next;
    }
    return 0;
}

/** Remove node behind 'before' node and save removed value in result **/
int remove_before(const char *before, char *result) {
    struct Node *current = node_head;
    struct Node *prev = NULL, *prev_prev = NULL, *prev_prev_prev = NULL;
    struct Node *next;

    while (current != NULL) {
        if (strcmp(node_head->value, before) == 0) {
            // just return as nothing before node_head
            return 0;
        }
        next = getNextNode(current, prev);

        if (strcmp(before, current->value) == 0) {
            if (prev == node_head) {
                // reassign node_head as previous node is about to be deallocated
                node_head = current;
            }
            populate_remove_result(result, prev->value);

            // deallocate previous node address
            free(prev);
            prev = NULL;
            // update current and previous_previous node addresses
            current->xor_next_prev = XOR(prev_prev, next);
            if (prev_prev != NULL) {
                prev_prev->xor_next_prev = XOR(prev_prev_prev, current);
            }

            return 1;
        }
        prev_prev_prev = prev_prev;
        prev_prev = prev;
        prev = current;
        current = next;
    }
    return 0;
}

void print_list() {
    struct Node *current = node_head;
    struct Node *prev = NULL;
    struct Node *next;

    while (current != NULL) {
        printf("%s -> ", current->value);
        next = getNextNode(current, prev);
        prev = current;
        current = next;
    }
    printf("NULL\n");
}

/** Assign the given value to the result **/
void populate_remove_result(char *result, char *value) {
    // clear result of any content
    result[0] = '\0';
    for (int i = 0; i < strlen(value); i++) {
        char *ch = make_char(value[i]);
        // concatenate result with pointer to char (string) at current index in string
        strcat(result, ch);
    }
}

int main(int argc, char *argv[]) {
    insert_string("Alpha");
    insert_string("Bravo");
    insert_string("Charlie");
    insert_after("Bravo", "Delta");
    insert_before("Alpha", "Echo");
    print_list(); // Charlie -> Bravo -> Delta -> Echo -> Alpha

    char result[10];
    int ret;

    ret = remove_after("Delta", result);
    if (ret)
        printf("Removed: %s\n", result);

    ret = remove_before("Bravo", result);
    if (ret)
        printf("Removed: %s\n", result);

    ret = remove_string(result);
    if (ret)
        printf("Removed: %s\n", result);

    print_list();
}